import React from "react";

function Mention(props) {
  return (
    <>
      <div classNameName="bg-primary w-full">
        <div className="container mx-auto mt-2 text-center  text-white ">
          <div className="société mb-3">
            <h2 className="text-2xl">
              Informations concernant la SAS. La Boite A Dev
            </h2>
            <p>Raison social: SAS La Boite A Dev</p>
            <p>Forme juridique: Société par Actions Simplifiés</p>
            <p>N° de RCS: Troyes 912356813</p>
            <p>Capital: 100€</p>
            <p>Adresse: 14 Grande Ruelle 10100 CRANCEY</p>
            <p>Contact: 0603935298 / contact@laboiteadev.fr</p>
          </div>
          <div className="herberger mb-3">
            <h2 className="text-2xl">Informations concernant hébergeur</h2>
            <p>Nom: O2switch</p>
            <p>Adresse:222 Bd Gustave Flaubert, 63000 Clermont-Ferrand </p>
            <p>Téléphone:04 44 44 60 40</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Mention;
