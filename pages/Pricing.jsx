import React, { useEffect, useState } from 'react';
import Navigation from './components/Navigation';
import axios from 'axios';
import Head from 'next/head';
import Card from './components/Card';
import Footer from './components/Footer';

function Pricing(props) {
    const [data, setData] = useState(null);
 
    useEffect(()=>{
        axios.get('/pricing.json')
        .then((res) => setData(res.data));
    },[]);
    return (
<>
<Head>
          <title>La Boite A Dev</title>
          <meta
            name="description"
            content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier. "
          />
          <meta property="og:site_name" content="La Boite A Dev"/>
          <meta property="og:description" content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier."/>
          <meta property="og:url" content="https://laboiteadev.fr/Pricing"/>
          <meta property="og:title" content="La Boite A Dev" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
            <Navigation />
            <div className="bg-white dark:bg-gray-800">
                <div className="container px-3 py-4 mx-auto items-center" style={{"minHeight": "750px"}}>
                    <div className="flex flex-col items-center justify-center space-y-8 lg:mx-4 lg:flex-row lg:items-stretch lg:space-y-0">
                        {data &&<div className="flex md:flex-row flex-col">
                            {data.tarifs.map((tarifs,index)=>(
                            <Card key={index} packTitle={tarifs.packTitle} price={tarifs.price} composition1={tarifs.composition1} composition2={tarifs.composition2} composition3={tarifs.composition3} />))}
                        </div>}
                    </div>   
                </div>
            </div>
            <Footer />
            </>
    );
}

export default Pricing;