import Link from 'next/link';
import react from 'react';
import React from 'react';
import Logo from './Logo';
import {useState} from "react";

function Navigation(props) {
    const [rotate, setRotate]= useState(false);
    const handleMenuItems = ()=>{
        
        (rotate===false)?document.getElementById("ham").style.transform = "rotate(90deg)": document.getElementById("ham").style.transform = "rotate(0deg)";
    
        setRotate(!rotate);
        console.log('menu cliquer');
        document.getElementById('menu').classList.toggle('hidden');
    }
    return (
    <header className="text-gray-600 body-font shadow shadow-black " >
        <div className="container mx-auto flex flex-wrap lg:py-5 flex-col lg:flex-row items-center justify-between ">
            <div className="flex  justify-between">
                <div className="w-1/3">
                {/* <Link href="/" >
                    <a className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0"><Logo /></a>
                </Link> */}
                </div>
                
                <div className="flex lg:hidden w-1/3 justify-end" >
                    
                    <button  type="button" className=" text-gray-500 dark:text-gray-200 hover:text-gray-600 dark:hover:text-gray-400 focus:outline-none focus:text-gray-600 dark:focus:text-gray-400" aria-label="toggle menu" onClick={handleMenuItems} >
                        <svg viewBox="0 0 24 24" className="w-6 h-6 fill-current" width={20} height={20} id="ham">
                            <path fillRule="evenodd" d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"></path>
                        </svg>
                    </button>
                </div>
            </div>
            
                <nav className={"flex flex-col justify-end text-gray-600 capitalize dark:text-gray-300 lg:flex lg:px-16 lg:-mx-4 lg:flex-row lg:items-center hidden lg:justify-end"} id="menu" >
                <Link href="/" ><a className=" mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200 ">Accueil</a></Link>
                <Link href="/About" ><a className=" mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Qui sommes nous?</a></Link>
                <Link href="/Pricing" ><a className=" mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Nos tarifs</a></Link>
                {/* <Link href="/Products" ><a className=" mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Nos Produits</a></Link> */}
                <Link href="/Contact" ><a className=" mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Contact</a></Link>
            </nav>
        </div>
    </header>
    );
}

export default Navigation;