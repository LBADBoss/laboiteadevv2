import Link from 'next/link';
import React from 'react';

function Card(props) {
    return (
            <div className="flex flex-col w-full max-w-sm p-8 space-y-8 text-center bg-primary border-2 border-gray-200 rounded-lg lg:mx-4 dark:bg-gray-800 dark:border-gray-700">
                <div className="flex-shrink-0">
                    <h2 className="inline-flex items-center justify-center px-2 font-semibold tracking-tight text-blue-400 uppercase rounded-lg bg-gray-50 dark:bg-gray-700">
                        { props.packTitle }
                    </h2>
                </div>
                <div className="flex-shrink-0">
                        <span
                            className="pt-2 text-4xl font-bold text-gray-800 uppercase dark:text-gray-100"
                        >
                            { props.price }
                        </span>
                </div>
                <ul className="flex-1 space-y-4">
                    <li className="text-white dark:text-gray-400">
                        {props.composition1 }
                    </li>
                    <li className="text-white dark:text-gray-400">
                        {props.composition2}
                    </li>
                    <li className="text-white dark:text-gray-400">
                          { props.composition3 }
                    </li>
                
                  
                    
                </ul>
                <Link href="/Contact">
                <a 
                    className="inline-flex items-center justify-center px-4 py-2 font-semibold text-white uppercase transition-colors bg-blue-500 rounded-lg hover:bg-blue-700 focus:outline-none"
                >
                    Demande d'informations
                </a></Link>
            </div>
       

    );
}

export default Card;