import Image from 'next/image';
import React from 'react';

function Logo(props) {
    return (
        <Image src='/logo.svg' alt='logo de la société' width={100} height={100}   />
    );
}

export default Logo;