export default function (req, res) {
  require("dotenv").config();

  let nodemailer = require("nodemailer");
  const transporter = nodemailer.createTransport({
    port: 465,
    host: "mail.laboiteadev.fr",
    auth: {
      user: "contact@laboiteadev.fr",
      pass: process.env.MAILPASSWORD,
    },
    secure: true,
  });

  const mailData = {
    from: "contact@laboiteadev.fr",
    to: "contact@laboiteadev.fr",
    subject: `Demande du site`,
    text: req.body.message + " | Sent from: " + req.body.email,
    html: `<div>${req.body.message}</div><p>Sent from: ${req.body.email} ${req.body.name} ${req.body.company}</p>`,
  };

  transporter.sendMail(mailData, function (err, info) {
    if (err) console.log(err);
    else console.log(info);
  });

  console.log(req.body);
  res.send("success");
}
