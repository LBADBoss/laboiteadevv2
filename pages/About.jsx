import React from 'react';
import Navigation from './components/Navigation';
import Image  from 'next/image';
import triangle from '../public/img/triangle-wo.svg';
import quote from '../public/img/quote.svg';
import profil from '../public/img/profil.png';
import feuille from '../public/img/feuille.png';
import ptc from '../public/img/logoPTC.png';
import Link  from 'next/link';
import Footer from './components/Footer';
import Head from 'next/head';



function About(props) {
    
    return (
        <>
       <Head>
          <title>La Boite A Dev</title>
          <meta
            name="description"
            content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier. "
          />
          <meta property="og:site_name" content="La Boite A Dev"/>
          <meta property="og:description" content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier."/>
          <meta property="og:url" content="https://laboiteadev.fr/About"/>
          <meta property="og:title" content="La Boite A Dev" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
    <Navigation />
    <div className="container mx-auto max-w-2xl mt-4 ">
            <div id="review_section">
                <div className="">
                    <span className="triangle-w">
                        <Image src={triangle} alt="triangle"/>
                    </span>
                    <div className="card border-0 py-5 px-3 px-lg-5 mb-5 rounded bg-white shadow">
                        <div className="flex">
                            <div className="hidden  lg:block">
                                <Image src={quote} alt="quote" />
                            </div>
                            <div className="pl-md-4 pl-3">
                                
                                <p>Fort d'expérience dans différents coeur de métier, je suis capable de comprendre et de m'adapter à vos différentes contraintes métier afin de vous réalisé la solution la plus approprié à vos besoins.</p>
                                <p>Comme beaucoup aujourd'hui, nous sommes soucieux de l'avenir et disposons 
                                    <Link href="/Pricing">
                                    <a className="text-tertiary font-semibold text-shadow-sm"> d'offre éco
                                        responsable
                                    </a>
                                    </Link>
                                </p>
                                <div className="flex items-center">
                                    <div className="profil">
                                        <Image src={profil} className="rounded-pill border-1" alt="profil_picture" />
                                    </div>
                                    <div className="pl-3">
                                        
                                            <p >Rossi Sébastien</p>
                                            <p  className="text-muted">Directeur Général</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="bg-primary mx-auto mt-5 justify-center">
            <div className="py-8">
                <h1 className="text-tertiary font-semibold text-5xl text-center">Notre visions du développement</h1>
                    <div className="grid  grid-cols-1 md:grid-cols-3 mx-auto">
                        <div></div>
                        <div className=" mx-auto  mt-4 ">
                            <div className="flex justify-center   ">
                                <Image src={feuille} alt="feuille" className="inline w-1/6 justify-center " width={100} height={100} />
                            </div>
                            <div className="text-center  mt-5">
                                <div>
                                    <h3 className="text-center text-white text-3xl mt-3">Qu'est ce que le <span className="text-tertiary text-4xl font-semibold text-shadow-xl">Green IT:</span>
                                    </h3>
                                    <p className="text-center text-white mt-2">Le <span
                                    className="text-tertiary font-bold text-shadow-xl">green IT ou numérique responsable </span> est le
                                    fait de
                                    réduire la pollution générer par les systèmes d'informations. Pour atteindre ces
                                    objectifs,
                                    la green IT veut limiter la consommation d'énergie entre autre. </p>
                                </div>
                                <div className="text-center mt-3">
                                 <p className="text-center font-semibold text-tertiary text-3xl">En pratique:</p>
                                 <p className="text-center text-white mt-2">Nous vous conseillons sur des bonnes pratiques, mais aussi dans le
                                    choix
                                    d'hébergeur éco-responsable. Nous mettons tout en oeuvre pour réaliser des sites et applications le plus éco-responsable possible. Nous nous engageons au quotidien, dans notre politique générale, mais aussi lors de nos sessions de formations</p>
                                </div>
                                <div className="flex justify-center">
                                    <Image src={ptc} alt="logo signataire" className="inline w-1/6 justify-center" /> 
                                </div>
                            </div>
                        </div>
                    <div>
                </div>
            </div>
        </div>
    </div>
<Footer />

</>
    );
}

export default About;