import "../styles/globals.css";
import "../styles/ext.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
