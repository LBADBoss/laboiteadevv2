import Head from "next/head";

import Image from "next/image";
import macbook from "../public/img/macBook.png";
import stylo from "../public/img/stylo.png";
import crayon from "../public/img/crayon_blanc.png";
import web from "../public/img/web.png";
//import web_blanc from "../public/img/web_blanc.png";
import oav from "../public/img/oav.png";
//import oav_blanc from "../public/img/oav_blanc.png";

import Footer from "./components/Footer";
import Navigation from "./components/Navigation";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <div>
        <Head>
          <title>La Boite A Dev</title>
          <meta
            name="description"
            content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier. "
          />
          <meta property="og:site_name" content="La Boite A Dev" />
          <meta
            property="og:description"
            content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier."
          />
          <meta property="og:url" content="https://laboiteadev.fr" />
          <meta property="og:title" content="La Boite A Dev" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navigation />
        <div className="bg-primary  mx-auto px-10 py-10 w-full shadow-inner ">
          <div className="container mx-auto flex flex-col-reverse  md:flex-row ">
            <div className="mt-2 flex-1">
              <div className="hidden md:block">
                <p className="text-white text-7xl">Créateur de </p>
                <p className="text-white text-7xl font-bold">
                  solutions digitales
                </p>
                <div className="mt-5">
                  <p className="text-white text-3xl lg:pr-5 ">
                    Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier.
                  </p>
                </div>
                <div className="mt-8">
                  <p className="font-bold text-white text-2xl">#laboiteadev</p>
                </div>
              </div>
              <div className="mt-10 flex flex-col md:flex-row gap-6 md:gap-0">
                <Link href="/Pricing">
                  <a className="border rounded-xl text-white px-8 py-2 hover:bg-blue-200 text-center font-semibold ">
                    NOS TARIFS
                  </a>
                </Link>
                <Link href="/Contact">
                  <a className="border rounded-xl bg-white px-8 py-2 text-tertiary md:ml-2 font-semibold text-center hover:bg-gray-100 ">
                    NOUS CONTACTER
                  </a>
                </Link>
              </div>
            </div>
            <div className="flex justify-center flex-1 ">
              <Image src={macbook} alt="macbook" className="mx-auto" />
            </div>
          </div>
        </div>
        <div className=" block md:hidden ml-2">
          <p className="text-primary text-7xl">Créateur de </p>
          <p className="text-primary text-7xl font-bold">solutions digitales</p>
          <div className="border-l-4 pl-4 border-tertiary">
            <div className="mt-5">
              <p className="text-primary text-3xl lg:pr-5 ">
                Conception et développement de solutions numériques de la
                création de sites web sur mesure à la création
                d&apos;applications métier.
              </p>
            </div>
            <div className="mt-8">
              <p className="font-bold text-primary text-2xl">#laboiteadev</p>
            </div>
          </div>
        </div>
        <div className="bg-primary md:bg-white pb-8">
          <div className="mt-8 text-center">
            <p className="text-center md:text-7xl md:text-tertiary text-white font-semibold  text-3xl py-5">
              NOS PRESTATIONS
            </p>
            <div className="border-b-2 border-white flex justify-center  md:hidden w-2/3 mx-auto"></div>
          </div>
          <div className="grid md:grid-cols-3 grid-cols-1 mx-auto my-3 text-center ">
            <div className=" text-center    mx-auto">
              <Image
                src={stylo}
                className="md:inline hidden "
                width={150}
                height={150}
                alt="stylo"
              />

              <div className="justify-center">
                <p className="md:text-tertiary text-white text-center font-semibold text-3xl hidden">
                  PRINT
                </p>
              </div>
              <div className="mt-4 text-2xl text-white md:text-black">
                <ul>
                  <li>Création de logo</li>
                  <li>Création de charte graphiques</li>
                  <li>Rédaction de cahiers des charges</li>
                </ul>
              </div>
            </div>
            <div className=" text-center  mx-auto">
              <Image
                src={web}
                className="md:inline hidden"
                alt="web"
                width={150}
                height={150}
              />
              <div className="justify-center">
                <p className="md:text-tertiary  text-center font-semibold text-3xl hidden">
                  WEB
                </p>
              </div>
              <div className="mt-4 text-2xl text-white md:text-black">
                <ul>
                  <li>Conception de site web</li>
                  <li>Elaboration d’applications web</li>
                  <li>Conception de site e-commerce</li>
                </ul>
              </div>
            </div>
            <div className=" text-center  mx-auto">
              <Image
                src={oav}
                className="md:inline hidden"
                alt="oav"
                width={150}
                height={150}
              />
              <div className="justify-center">
                <p className="md:text-tertiary  md:text-center md:font-semibold text-3xl hidden">
                  OAV
                </p>
              </div>
              <div className="mt-4 text-2xl text-white md:text-black">
                <ul>
                  <li>Conception d&apos;application métiers</li>
                  <li>Mise en place d&apos;ERP de comptabilité</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="md:text-center mt-4 hidden md:block">
            <Link href="/Contact">
              <a className="rounded-xl border px-2 py-3 bg-quatro my-5 ">
                Vous avez un projet qui ne figure pas dans cette liste ?
                <span className="text-tertiary mt-2"> Cliquez-ici </span>{" "}
              </a>
            </Link>
          </div>
          <div className="text-center mt-8 mb-5">
            <Link href="/Pricing">
              <a className=" border-4 md:border-primary border-white rounded-full md:bg-white px-8 py-2 md:text-primary ml-2 font-semibold text-center hover:bg-gray-100 text-3xl text-white">
                EN SAVOIR PLUS
              </a>
            </Link>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
}
