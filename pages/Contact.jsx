import React from 'react';
import Navigation from './components/Navigation';
import Footer from './components/Footer';
import  Head  from 'next/head';
import { useState } from 'react';

function Contact(props) {
    
    
        const [name, setName] = useState('')
        const [company , setCompany] = useState('')
        const [email, setEmail] = useState('') //
        const [subject, setSubject] = useState('')
        const [message , setMessage] = useState('');
        const [submitted, setSubmitted] = useState(false)
        

        const handleSubmit = (e)=>{
            e.preventDefault();
            //console.log('Envoyer');

            let data ={
                name,
                company,
                email,
                subject,
                message
            }
            fetch('/api/contact', {
                method: 'POST',
                headers:{
                    'Accept':'Application/json, text/explain,*/*',
                    'Content-Type':'Application/json',
                },
                body: JSON.stringify(data)}).then((res)=>{
                    //console.log('Réponse recu')
                    if(res.status === 200) {
                        //console.log('Response succeeded!')
                        setSubmitted(true) 
                        setName('')
                        setCompany('')
                        setEmail('')
                        setSubject('')
                        setMessage('')
        }})
        };
    

    return (
    <>
        <div>
        <Head>
          <title>La Boite A Dev</title>
          <meta
            name="description"
            content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier. "
          />
          <meta property="og:site_name" content="La Boite A Dev"/>
          <meta property="og:description" content="Sociéte de Conception et développement de solutions numériques de la
                    création de sites web sur mesure à la création
                    d'applications métier."/>
          <meta property="og:url" content="https://laboiteadev.fr/Contact"/>
          <meta property="og:title" content="La Boite A Dev" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
            <Navigation />
            <div className="text-gray-600 body-font relative">
                <div className="container px-5 py-24 mx-auto">
                    <div className="flex flex-col text-center w-full mb-12">
                        <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Contacter-nous</h1>
                        <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Pour toutes questions concernant votre futur projet ou sur un de nos projets en cours n'hésitez pas nous contacter, notre équipe vous répondra sous 48h </p>
                    </div>

                    <form className="lg:w-1/2 md:w-2/3 mx-auto"  method="post" >
                        <div className="flex flex-wrap -m-2">
                            <div className="p-2 w-1/2">
                                <div className="relative">
                                    <label htmlFor="name" className="leading-7 text-sm text-gray-600">Nom Prénoms</label>
                                    <input required onChange={(e)=>{setName(e.target.value)}} type="text" id="name" name="name" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={name} />
                                </div>
                            </div>
                            <div className="p-2 w-1/2">
                                <div className="relative">
                                    <label htmlFor="company" className="leading-7 text-sm text-gray-600">Nom de la société</label>
                                    <input  onChange={(e)=>{setCompany(e.target.value)}} type="text" id="company" name="company" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={company} />
                                </div>
                            </div>
                        </div>
                        <div className="p-2 w-full">
                            <div className="relative">
                                <label htmlFor="email" className="leading-7 text-sm text-gray-600">Email</label>
                                <input required onChange={(e)=>{setEmail(e.target.value)}} type="email" id="email" name="email" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"  value={email}/>
                            </div>
                        </div>
                        <div className="flex flex-wrap justify-center">
                            <div className="p-2 w-1/2">
                                <div className="relative">
                                    <label htmlFor="subject" className="leading-7 text-sm text-gray-600">Objet</label>
                                    <select onChange={(e)=>{setSubject(e.target.value)}} id="subject" name="subject" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" >
                                        <option value="Demande de devis"> Demande de devis </option>
                                        <option value="Demande d'information">Demande d'information </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="p-2 w-full">
                            <div className="relative">
                                <label htmlFor="message" className="leading-7 text-sm text-gray-600">Message</label>
                                <textarea onChange={(e)=>{setMessage(e.target.value)}} id="message" name="message" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out" value={message}></textarea>
                            </div>
                        </div>
                        <div className="p-2 w-full">
                            <button onClick={(e)=>{handleSubmit(e)}} type="submit" className="flex mx-auto text-white bg-primary border-0 py-2 px-6 focus:outline-none hover:bg-blue-200 rounded text-lg" >Envoyer</button>
                        </div>
                    </form>
                    <div className="p-2 w-full pt-8 mt-8 border-t border-gray-200 text-center">
                        <a className="text-indigo-500">contact@laboiteadev.fr</a>
                        <p className="leading-normal my-5">SAS.La Boite A Dev
                            <br/>14 grande ruelle
                            <br/>10100 CRANCEY
                            <br/>RCS Troyes 912356813
                        </p>
                        {/* <span className="inline-flex">
                            <a className="text-gray-500">
                                <svg fill="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                                <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                                </svg>
                            </a>
                            <a className="ml-4 text-gray-500">
                                <svg fill="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                                <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
                                </svg>
                            </a>
                        </span> */}
                    </div>
                </div>
            </div>
        </div> 
        <Footer/> 
    </>
    );
}

export default Contact;