/** @type {import('tailwindcss').Config} */
const colors = require("tailwindcss/colors");
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        transparent: "transparent",
        current: "currentColor",
        primary: {
          DEFAULT: "#1E9CD7",
        },
        blue: colors.blue,
        secondary: {
          DEFAULT: "#BF5B04",
        },
        tertiary: {
          DEFAULT: "#F28322",
        },
        quatro: {
          DEFAULT: "#CEE8F2",
        },
        white: colors.white,
        gray: colors.gray,
        black: colors.black,
      },
    },
  },
  plugins: [],
};
